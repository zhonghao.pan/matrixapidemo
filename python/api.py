#!/usr/bin/python3


"""
    Matrix api python 示例
    ecdsa库下载地址: https://github.com/pzhhrf/python-ecdsa (备注:改了VerifyKey类中,from_pem函数,添加一个默认hash函数)
"""

import requests,hashlib
from email.utils import formatdate
from ecdsa import SigningKey,NIST256p
from ecdsa import VerifyingKey
from ecdsa.util import sigencode_der
import random,base64,json

private_string = """-----BEGIN EC PRIVATE KEY-----
MHcCAQEEICaPzTYnm0T3Q4DbiQB5RMTFhPvVFGNc6fVpZahDdwh8oAoGCCqGSM49
AwEHoUQDQgAEeEdpucdPiockBaAfGSAsJvQFQA3/LZuVLG+/zymmkUC1NjqWcMUp
+tyArB0vSpZI0BuSQrvtEjZvZDMgYx2EWw==
-----END EC PRIVATE KEY-----"""

public_string = """-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEeEdpucdPiockBaAfGSAsJvQFQA3/
LZuVLG+/zymmkUC1NjqWcMUp+tyArB0vSpZI0BuSQrvtEjZvZDMgYx2EWw==
-----END PUBLIC KEY-----"""

# host = "http://apitest.matrixport.com:8081"
host = "http://127.0.0.1:8091"

access_key_id = "B_1bcb4959-3b4d-4a0f-a0e7-006347a2ae7e"

date = formatdate(timeval=None, localtime=False, usegmt=True)

GHeaders = {
    "Accept":"application/json",
    "Content-SHA256" : "",
    "Content-Type":"application/json",
    "Date" : date,
}

#返回headers
def makeSign(path,body = None,method = "GET"):
    if body is not None:
        GHeaders["Content-SHA256"] = base64.b64encode(hashlib.sha256(body.encode("utf-8")).digest()).decode()

    mydict = {
        "x-bfs-signature-nonce":str(random.randint(1,10000000)),
        "x-bfs-bp":"test",
    }
    headers = dict(GHeaders,**mydict)
    mystr = "\n".join(["%s:%s" %(k,v) for k,v in sorted(mydict.items(), key = lambda x : x[0])])
    signStr = "{0}\n{1}\n{2}\n{3}\n{4}\n{5}\n{6}".format(method,headers.get("Accept"),headers.get("Content-SHA256"),
        headers.get("Content-Type"),headers.get("Date"),mystr,path)

    sk = SigningKey.from_pem(private_string.encode("utf-8"),hashfunc = hashlib.sha256)
    #这里注意,java ecdsa库sign过后是ans1 der格式, python的不是,需要调用signcode_der才行
    #https://crypto.stackexchange.com/questions/57731/ecdsa-signature-rs-to-asn1-der-encoding-question
    signature = base64.b64encode(sk.sign(signStr.encode("utf-8"),sigencode = sigencode_der)).decode()
    auth = "bfs %s:%s" %(access_key_id,signature)
    headers["Authorization"] = auth
    return headers


#get样例
def get_exp():
    path = "/trader/v2/prices?pair=%s&cashamount=%s&side=0&type=spot" %("BTCUSD","5000")
    headers = makeSign(path)
    hr = requests.get(host + path,headers = headers)
    con = json.loads(hr.text)
    print(con)
    post_exp(con.get("price_id"))
    

#下单
def post_exp(price_id):
    path =  "/trader/v2/orders/rfq/create"
    body = {
        "business_order_id" : "test" + str(random.randint(1,10000000)),
        "pair" : "BTCUSD",
        "amount" : "0.1",
        "price_id" : price_id,
        "side" : "0",
    }
    headers = makeSign(path,body = json.dumps(body),method = "POST")
    hr = requests.post(host + path,headers = headers, data = json.dumps(body))
    con = json.loads(hr.text)
    print(con)

def main():
    get_exp()
    

if __name__ == '__main__':
    main()