package Matrix;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.*;

/**
 * MatrixApi java demo
 * 示例为api函数根据金额询价,主要是为了描述加密算法和使用
 */
public class App {

    private static final String AccessKeyId = "B_1bcb4959-3b4d-4a0f-a0e7-006347a2ae7e";

    //你的私钥pem读出来的字符串
    private static final String privateKey = "MIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgJo/NNiebRPdDgNuJAHlExMWE+9UUY1zp9WllqEN3CHygCgYIKoZIzj0DAQehRANCAAR4R2m5x0+KhyQFoB8ZICwm9AVADf8tm5Usb7/PKaaRQLU2OpZwxSn63ICsHS9KlkjQG5JCu+0SNm9kMyBjHYRb";

    private static final String publicKey = "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEeEdpucdPiockBaAfGSAsJvQFQA3/" +
            "LZuVLG+/zymmkUC1NjqWcMUp+tyArB0vSpZI0BuSQrvtEjZvZDMgYx2EWw==";

    private static final String host = "http://apitest.matrixport.com:8081";
//    private static final String host = "http://127.0.0.1:8091";

    public static void main(String[] args) {
        traderByPrice();
    }

    public static void genHeader(HttpRequestBase request, String path, String body) {
        Random random = new Random();
        //API自定义header内容
        Map<String, String> map = new HashMap<>();
//        map.put("x-bfs-signature-nonce", String.valueOf(random.nextInt(10000)));
        map.put("x-bfs-signature-nonce", "1");
        map.put("x-bfs-bp", "test");
        //自定义需要加密的字符串
        Date date = new Date();
        request.setHeader("Accept", "application/json");
        if (request.getMethod().equals("GET")) {
            request.setHeader("Content-SHA256", "");
        } else {
            request.setHeader("Content-SHA256", Base64.getEncoder().encodeToString(DigestUtils.sha256(body)));
        }
        request.setHeader("Content-Type", "application/json");
//        request.setHeader("Date", date.toString());
        request.setHeader("Date", "Fri Apr 26 20:44:24 CST 2019");
        //把自定义header合并
        map.forEach((k, v) -> {
            request.setHeader(k, v);
        });
        //把自定义的header进行按照字母大小排序
        Object[] key = map.keySet().toArray();
        Arrays.sort(key);
        //构造需要签名的字符串
        StringBuilder sb = new StringBuilder();
        final String LF = "\n";
        sb.append(request.getMethod()).append(LF)
                .append(request.getFirstHeader("Accept").getValue()).append(LF)
                .append(request.getFirstHeader("Content-SHA256").getValue()).append(LF)
                .append(request.getFirstHeader("Content-Type").getValue()).append(LF)
                .append(request.getFirstHeader("Date").getValue()).append(LF);
        //循环把自定义内容添加到签名字符串中
        for (int i = 0; i < key.length; i++) {
            sb.append(String.format("%s:%s", key[i], map.get(key[i]))).append(LF);
        }
        sb.append(path);
        System.out.println("begin===" + sb.toString());
        String sign = Sign(sb.toString());
        System.out.println("after===" + sign);
        String auth = String.format("bfs %s:%s", AccessKeyId, sign);
        //把签名放入header
        request.setHeader("Authorization", auth);
    }

    //按照金额询价
    public static void traderByPrice() {
        String pair = "BTCUSD";
        String cashamount = "5000";
        String path = String.format("/trader/v2/prices?pair=%s&cashamount=%s&side=0&type=spot", pair, cashamount);
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(host + path);
        //构造header
        genHeader(request, path, null);
        String result = exec(request);
        if (result == null) {
            System.out.println("error");
            System.exit(0);
        }
        JSONObject json = JSONObject.parseObject(result);
        if (json.getInteger("code") == 0) {
            //下单
            postOrder(json.getString("price_id"));
        }
    }

    //签名函数,把字符串str使用私钥签名,返回base64以后的值
    public static String Sign(String str) {
        String rtn = null;
        try {
            PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKey));
            KeyFactory keyFactory = KeyFactory.getInstance("EC");
            PrivateKey privKey = keyFactory.generatePrivate(pkcs8EncodedKeySpec);
            Signature ecdsaSign = Signature.getInstance("SHA256withECDSA");
            ecdsaSign.initSign(privKey);
            ecdsaSign.update(str.getBytes("UTF-8"));
            rtn = Base64.getEncoder().encodeToString(ecdsaSign.sign());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rtn;
    }

    //下单接口
    public static void postOrder(String price_id) {
        String path = "/trader/v2/orders/rfq/create";
        HttpPost post = new HttpPost(host + path);
        JSONObject js = new JSONObject();
        js.put("business_order_id", "123ABCD");
        js.put("pair", "BTCUSD");
        js.put("amount", "0.01");
        js.put("price_id", price_id);
        js.put("side", "0");
        StringEntity entity = new StringEntity(js.toString(), Charset.forName("UTF-8"));
        entity.setContentEncoding("UTF-8");
        entity.setContentType("application/json");
        post.setEntity(entity);
        genHeader(post, path, js.toString());
        String result = exec(post);
    }

    public static String exec(HttpRequestBase request) {
        HttpClient client = HttpClientBuilder.create().build();
        try {
            HttpResponse response = client.execute(request);
            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer result = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
                result.append(line);
            }
            System.out.println("result:" + result.toString());
            return result.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
